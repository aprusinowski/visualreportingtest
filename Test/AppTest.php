<?php

namespace VR\Test;

use VR\App\Converter;

class AppTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers            \VR\App\Converter::__construct
     * @uses              \VR\App\Converter
     */
    public function testConstructor()
    {
        $c = new Converter();
        $this->assertInstanceOf('\\VR\\App\\Converter', $c);
        return $c;
    }

    /**
     * @dataProvider      romanProvider
     * @depends           testConstructor
     */
    public function testConvert($arabic, $expected, Converter $c)
    {
        $roman = $c->convert($arabic);
        $this->assertRegExp('/^[IVXLCDM]+$/', $roman);
        $this->assertEquals($expected, $roman);
    }

    /**
     * @dataProvider      romanFileProvider
     * @depends           testConstructor
     */
    public function testFileConvert($arabic, $expected, Converter $c)
    {
        $roman = $c->convert($arabic);
        $this->assertRegExp('/^[IVXLCDM]+$/', $roman);
        $this->assertEquals($expected, $roman);
    }

    /**
     * @expectedException \VR\Exception\BadArgumentException
     * @depends           testConstructor
     */
    public function testNegativeException(Converter $c)
    {
        $c->convert(-1);
    }

    /**
     * @expectedException \VR\Exception\BadArgumentException
     * @depends           testConstructor
     */
    public function testZeroException(Converter $c)
    {
        $c->convert(0);
    }

    /**
     * @expectedException \VR\Exception\BadArgumentException
     * @depends           testConstructor
     */
    public function testTooBigException(Converter $c)
    {
        $c->convert(4000);
    }

    /**
     * @expectedException \VR\Exception\BadArgumentException
     * @depends           testConstructor
     */
    public function testNaNException(Converter $c)
    {
        $c->convert('test');
    }


    /**
     * @expectedException \VR\Exception\BadArgumentException
     * @depends           testConstructor
     */
    public function testFloatException(Converter $c)
    {
        $c->convert(0.7);
    }

    public function romanProvider()
    {
        return array(
            array(1, 'I'),
            array(2, 'II'),
            array(3, 'III'),
            array(4, 'IV'),
            array(5, 'V'),
            array(6, 'VI'),
            array(7, 'VII'),
            array(8, 'VIII'),
            array(9, 'IX'),
            array(10, 'X'),
            array(11, 'XI'),
            array(15, 'XV'),
            array(19, 'XIX'),
            array(20, 'XX'),
            array(21, 'XXI'),
            array(40, 'XL'),
            array(50, 'L'),
            array(77, 'LXXVII'),
            array(99, 'XCIX'),
            array(100, 'C'),
            array(150, 'CL'),
        );
    }

    public function romanFileProvider()
    {
        return array(array(1, 'I')); // To skip this test, it's very long...

        $file = file('Test/romans.txt');
        $out = array();
        foreach ($file as $number) {
            $number = explode('=',trim($number));
            $out[] = array($number[0], $number[1]); // not plain "$out[] = $number", because of "4=IV=IIII" etc.
        }
        return $out;
    }
}
