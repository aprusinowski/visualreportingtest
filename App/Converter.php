<?php

namespace VR\App;

use VR\Exception\BadArgumentException;

class Converter
{
    const MIN = 1;
    const MAX = 3999;

    public function convert($arabic) {
        $arabic = $this->assertInputValue($arabic);

        list($thousands, $hundreds, $tens, $ones) = $this->getPositions($arabic);

        return
            $this->convertDigit((int)$thousands, 'M', '',  '')  .   // THOUSANDS
            $this->convertDigit((int)$hundreds,  'C', 'D', 'M') .   // HUNDREDS
            $this->convertDigit((int)$tens,      'X', 'L', 'C') .   // TENS
            $this->convertDigit((int)$ones,      'I', 'V', 'X');    // ONES
    }

    private function assertInputValue($value) {
        if (!is_numeric($value)) {
            throw new BadArgumentException('Input value must be a decimal');
        }
        if ($value < self::MIN) {
            throw new BadArgumentException(sprintf('Input value must be %d or greater', self::MIN));
        }
        if ($value > self::MAX) {
            throw new BadArgumentException(sprintf('Input value must be %d or less', self::MAX));
        }
        if (round($value) != $value) {
            throw new BadArgumentException('Input value must be an integer');
        }
        return (int)$value;
    }

    private function getPositions($number) {
        return str_split(str_pad($number,4, '0', STR_PAD_LEFT));
    }

    private function mergeRoman($main, $aside, $times) {
        if ($times < 0) {
            return str_repeat($aside, -$times) . $main;
        } else if ($times == 0) {
            return $main;
        } else {
            return $main . str_repeat($aside, $times);
        }
    }

    private function convertDigit($digit, $ones, $fives, $tens) {
        if ($digit == 0) { return ''; }
        if ($digit <= 3) {
            return $this->mergeRoman('', $ones, $digit);
        } else if ($digit >= 4 && $digit <= 8) {
            return $this->mergeRoman($fives, $ones, $digit - 5);
        } else if ($digit == 9) {
            return $this->mergeRoman($tens, $ones, -1);
        }
    }
}
