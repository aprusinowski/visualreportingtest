<?php

namespace VR\App;

class Autoloader
{
    /** @var  string */
    private $namespace;

    /** @var string */
    private $path;

    /** @var bool */
    private $valid = false;

    /**
     * @param $namespace
     * @param string $path
     */
    public function __construct($namespace, $path = '') {
        $this->namespace = $namespace;
        $this->path = '';
        $this->valid = true;
    }

    /**
     * @param string $className
     * @return bool
     */
    public function autoload($className) {
        if (substr($className,0,strlen($this->namespace) !== $this->namespace)) {
            $path = ltrim( substr($className, strlen($this->namespace)), '\\') . $this->path . '.php';
            if (file_exists($path)) {
                require_once($path);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function register()
    {
        if ($this->valid) {
            spl_autoload_register(array($this, 'autoload'));
            $this->valid = false;
        }
    }
}
