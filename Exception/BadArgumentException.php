<?php
namespace VR\Exception;

class BadArgumentException extends \Exception
{
    /**
     * @param string $message
     */
    public function __construct($message) {
        parent::__construct('Invalid argument: '.$message);
    }
}