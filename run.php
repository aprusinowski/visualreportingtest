<?php

require_once 'autoload.php';

if (count($GLOBALS['argv']) < 2) { throw new Exception('Numeric input is required'); }
$converter = new VR\App\Converter();
echo $converter->convert($GLOBALS['argv'][1]) . "\n";